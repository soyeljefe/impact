angular.module('myApp', []).controller('myCtrl', function($scope, $http) {

    $scope.beers = {};
    $scope.addedBeers = [];

    $scope.init = function()
    {
        // Load all data, and display it
        this.data_load();
    };

    $scope.data_load = function()
    {
        console.log("Loading data");

        $http({
            method: 'GET',
            url: 'https://api.punkapi.com/v2/beers'
        }).then(function successCallback(response) {
            $scope.beers = {};
            $scope.beers = response;

            for(var i = 0; i < $scope.addedBeers.length; i++)
            {
                $scope.beers.data.push($scope.addedBeers[i]);
            }

            console.log($scope.beers);

        }, function errorCallback(response) {
            console.log("Something horrible went wrong - handle!")
        });
    };

    // Toggle beer description
    $scope.activeButton = function(id) {
        var myEl = angular.element( document.querySelector( '#beer_' + id ) );
        if(myEl.hasClass('active'))
        {
            myEl.removeClass('active');
        }
        else
        {
            myEl.addClass('active');
        }
    };

    $scope.beer_remove = function(id)
    {
        event.stopPropagation();
        console.log(id);
    }

    $scope.add_beer_modal_open = function()
    {
        $scope.add_beer_name = "";
        $scope.add_beer_description = "";

        var myEl = angular.element( document.querySelector( '.modal' ) );
        myEl.removeClass("hidden");
    }

    $scope.add_beer_modal_close = function()
    {
        var myEl = angular.element( document.querySelector( '.modal' ) );
        myEl.addClass("hidden");
    }

    $scope.add_beer = function()
    {
        var _id = Math.ceil($scope.getRandomArbitrary());
        var _name = $scope.add_beer_name;
        var _description = $scope.add_beer_description;

        var _obj = {};
        _obj = {
            id: _id,
            name: _name,
            description: _description
        };

        $scope.addedBeers.push(_obj);
        $scope.data_load();

        $scope.add_beer_modal_close();
    }

    // Random number for id
    $scope.getRandomArbitrary = function() {
        return Math.random() * (10000 - 100) + 100;
    }

});